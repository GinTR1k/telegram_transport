FROM python:3.8.0-alpine

RUN apk add --no-cache \
    gcc musl-dev libffi-dev libtool g++ make && \
    adduser -D telegram_transport

WORKDIR /home/telegram_transport

COPY telegram_transport telegram_transport
COPY app.py boot.sh requirements.txt ./

RUN chmod +x boot.sh && \
    chown -R telegram_transport:telegram_transport . && \
    pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["./boot.sh"]
