import asyncio
from telegram_transport import start_app
from telegram_transport.config import Config


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    rmq_connection = loop.run_until_complete(
        start_app(loop=loop, config=Config))

    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(rmq_connection.close())
