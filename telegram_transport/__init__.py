from aio_pika import connect_robust
from aiogram import Bot
from telegram_transport.config import Config
from telegram_transport.consumer import RMQConsumer


async def start_app(loop, config=Config):
    rmq_connection = await connect_robust(
        config.RMQ_URL, timeout=10, loop=loop)

    telegram_bot = Bot(token=config.TELEGRAM_TOKEN, timeout=5)

    channel = await rmq_connection.channel()

    exchange = await channel.declare_exchange(
        config.RMQ_EXCHANGE_NAME, durable=True)
    queue = await channel.declare_queue(
        config.RMQ_QUEUE, durable=True, arguments={
            'x-dead-letter-exchange': config.RMQ_EXCHANGE_NAME,
            'x-dead-letter-routing-key': 'error'
        })

    await queue.bind(exchange, config.RMQ_ROUTING_KEY)

    queue_consumer = RMQConsumer(
        queue=queue, bot=telegram_bot, config=config)
    await queue_consumer.run()
