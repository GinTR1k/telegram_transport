import json
from aio_pika import Queue, IncomingMessage
from aiogram import Bot


class RMQConsumerBase:
    __slots__ = ('_queue', '_bot', '_config')

    def __init__(self, queue: Queue, bot: Bot, config):
        self._queue = queue
        self._bot = bot
        self._config = config

    async def run(self):
        await self._queue.consume(self.on_message)

    @property
    def queue(self) -> Queue:
        return self._queue

    @property
    def bot(self) -> Bot:
        return self._bot

    @staticmethod
    def _parse_message_body(message: IncomingMessage) -> dict:
        """ Метод для парсинга JSON-тела сообщения """
        try:
            return json.loads(message.body)
        except Exception:
            raise ValueError('Message is not JSON!')

    async def on_message(self, message: IncomingMessage):
        """ Handler на сообщения """
        raise NotImplemented('This is base class')
