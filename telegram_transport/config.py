from environs import Env


env = Env()
env.read_env()


class Config:
    DEBUG: bool = env.bool('DEBUG', True)
    WORKING: bool = env.bool('WORKING', True)

    RMQ_URL: str = env.str('RMQ_URL')
    RMQ_QUEUE: str = env.str(
        'RMQ_QUEUE', 'my_syper_telegram_transport_delivery_bot')
    RMQ_ROUTING_KEY: str = env.str('RMQ_ROUTING_KEY', 'default')
    RMQ_EXCHANGE_NAME: str = env.str('RMQ_EXCHANGE_NAME', 'telegram_transport')

    TELEGRAM_TOKEN: str = env.str('TG_TOKEN')
    TELEGRAM_DEBUG_USER: str = env.int('TG_DEBUG_USER')
