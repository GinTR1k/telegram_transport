from aio_pika import IncomingMessage
from telegram_transport.bases.consumer import RMQConsumerBase


class RMQConsumer(RMQConsumerBase):
    async def on_message(self, message: IncomingMessage):
        """ Handler на сообщения """
        async with message.process():
            telegram_message = self._parse_message_body(message)

            if self._config.DEBUG:
                config = self._config
                telegram_message.update({
                    'chat_id': config.TELEGRAM_DEBUG_USER})

            if self._config.WORKING or self._config.DEBUG:
                await self.bot.send_message(**telegram_message)
